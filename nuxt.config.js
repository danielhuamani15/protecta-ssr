const development = process.env.NODE_ENV !== 'production'
module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' },
      {
        rel: 'apple-touch-icon',
        sizes: '57x57',
        href: '/favicon/icon-57x57.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon/favicon-16x16.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: '/favicon/favicon-96x96.png'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '##ED6F2F', height: '3px', background: '##ED6F2F' },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/styles/style.scss',
    '@/assets/main/main.scss',
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/axios',
    { src: '~/plugins/slider', mode: 'client' },
    '~/plugins/fontawesome.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
  ],
  layoutTransition: 'page',
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    // '@nuxtjs/style-resources',
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'cookie-universal-nuxt',
    '@nuxtjs/sentry'
  ],
  sentry: {
    dsn: 'https://55b90f11b3cc46b68efe43556a706b77@sentry.io/1860144', // Enter your project's DSN here
    config: {
      disabled: !development
    } // Additional config
  },
  styleResources: {
    scss: [
      '@/assets/styles/style.scss',
      '@/assets/main/main.scss' // use underscore "_" & also file extension ".scss"
    ]
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: development
      ? 'http://weeare.localhost:8000/api_v1'
      : 'https://weeare-api.weeare.pe/api_v1'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  server: {
    port: 8004, // default: 3000
    host: 'localhost' // default: localhost
  }
}
