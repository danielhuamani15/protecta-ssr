import Cookies from 'js-cookie'
export const state = () => {
  return {
    baseUrlMedia: 'https://weeare.pe'
  }
}
export const getters = {
  BaseUrlMedia(state) {
    return state.baseUrlMedia
  }
}
export const actions = {
  nuxtServerInit({ commit }, { req, app }) {
    const auth = app.$cookies.get('token') ? app.$cookies.get('token') : null
    const firstName = app.$cookies.get('first_name')
    const lastName = app.$cookies.get('last_name')
    console.log('sssssssssssss')
    commit('auth/setFullName', `${firstName} ${lastName}`)
    commit('auth/setAuth', auth)
  }
}
