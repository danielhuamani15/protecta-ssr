import Cookies from 'js-cookie'

const TOKEN = 'token'
const EMAIL = 'email'
const DOCUMENT = 'num_document'
const FIRST_NAME = 'first_name'
const LAST_NAME = 'last_name'

export function jwtDecode(token) {
  const base64Url = token.split('.')[1]
  const base64 = base64Url.replace('-', '+').replace('_', '/')
  const dataUser = JSON.parse(window.atob(base64))
  return dataUser
}

export function setTokenData(token, user) {
  const jsonData = jwtDecode(token)
  const email = jsonData.email
  const numDocument = jsonData.num_document
  setToken(token)
  setDocument(numDocument)
  setEmail(email)
  setFirstName(user.first_name)
  setLastName(user.last_name)
  // setExpire(jsonData.exp)
}

export function setToken(token) {
  Cookies.set(TOKEN, token)
}
export function setFirstName(firstName) {
  Cookies.set(FIRST_NAME, firstName)
}
export function setLastName(lastName) {
  Cookies.set(LAST_NAME, lastName)
}

export function setEmail(email) {
  Cookies.set(EMAIL, email)
}

export function setDocument(numDocument) {
  Cookies.set(DOCUMENT, numDocument)
}

export function getToken() {
  return Cookies.get(TOKEN)
}

export function getEmail() {
  return Cookies.get(EMAIL)
}

export function getDocument() {
  return Cookies.get(DOCUMENT)
}

export function removeToken() {
  return Cookies.remove(TOKEN)
}

export function removeEmail() {
  return Cookies.remove(EMAIL)
}

export function removeDocument() {
  return Cookies.remove(DOCUMENT)
}
// function setExpire (expire) {
//   if (!expire) { return null; }
//   const date = new Date(0)
//   date.setUTCSeconds(token.exp)
//   return date;
// }

export function isAuthenticated() {
  const idToken = getToken()
  return !!idToken
}

export function authLogout() {
  removeToken()
  removeEmail()
  removeDocument()
}

// email: "da@da.com"
// exp: 1569892835
// num_document: "23232"
// orig_iat: 1569288035
// user_id: 54062
// username: "23232"
